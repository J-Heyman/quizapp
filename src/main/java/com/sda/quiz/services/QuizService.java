package com.sda.quiz.services;

import com.sda.quiz.models.QuizQuestion;
import com.sda.quiz.repositories.QuizRepo;
import lombok.Data;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class QuizService {
    private List<String> categoriesList;
    private List<QuizQuestion> allCategoryQuestions;
    private String category;
    private double numberOfQuestions;
    private double correctAnswers;
    private double wrongAnswers;

    public QuizService() {
        this.correctAnswers = 0;
        this.wrongAnswers = 0;
    }

    public List<String> getCategoriesNames() {
        this.categoriesList = QuizRepo.getListOfFiles().stream()
                .map(file -> file.getName().substring(0, file.getName().lastIndexOf('.')))
                .collect(Collectors.toList());
        return this.categoriesList;
    }

    public void setCategory(String category) {
        this.allCategoryQuestions = new ArrayList<>(QuizRepo.getCategoryQuestionsList(category));
        this.category = category;
    }

    public Set<QuizQuestion> getQuestions() {
        Random rand = new Random();
        Set<QuizQuestion> selectedQuestions = new HashSet<>();
        while (selectedQuestions.size() < numberOfQuestions) {
            selectedQuestions.add(allCategoryQuestions.get(rand.nextInt(allCategoryQuestions.size() - 1)));
        }
        return selectedQuestions;
    }

    public void addCorrectAnswer() {
        this.correctAnswers++;
    }

    public void addWrongAnswer() {
        this.wrongAnswers++;
    }

    public double countRate() {
        return correctAnswers / numberOfQuestions * 100d;
    }
}
