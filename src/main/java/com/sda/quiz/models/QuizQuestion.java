package com.sda.quiz.models;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class QuizQuestion {
    private String question;
    private Map<Integer, String> answers = new HashMap<>();

    public void addAnswer(Integer key, String answer) {
        this.answers.put(key, answer);
    }
}
