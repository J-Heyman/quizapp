package com.sda.quiz.cli;

import com.sda.quiz.services.QuizService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class QuizCli {

    private static QuizService service = new QuizService();

    public void runApp() {
        printTitle();
        printMenu();
        readCategoryIndex();
        readNumberOfQuestions();
        printQuestions();
    }

    private void printQuestions() {
        service.setCorrectAnswers(0);
        service.setWrongAnswers(0);

        service.getQuestions().forEach(q -> {
            List<String> mixedQuestions = new ArrayList<>(q.getAnswers().values());
            Collections.shuffle(mixedQuestions);
            String userAnswer;
            int readAnswer = -1;

            System.out.println("══════════════════════════════");
            System.out.println(q.getQuestion());

            for (int i = 0; i < q.getAnswers().size(); i++) {
                System.out.printf("%s) %s %n", i + 1, mixedQuestions.get(i));
            }
            System.out.println("──────────");
            System.out.print("Answer ► ");

            while (readAnswer < 0 || readAnswer > mixedQuestions.size() - 1) {
                readAnswer = readNumberFromUser() - 1;
                if (readAnswer < 0 || readAnswer > mixedQuestions.size() - 1) {
                    System.out.println("Wrong number. Try again!");
                    System.out.println("Answer ► ");
                }
            }
            userAnswer = mixedQuestions.get(readAnswer);


            if (userAnswer.equals(q.getAnswers().get(1))) {
                service.addCorrectAnswer();
            } else {
                service.addWrongAnswer();
            }

            System.out.println("► Your Answer is: " + userAnswer);
            System.out.println("► Correct Answer is: " + q.getAnswers().get(1));
        });

        System.out.println("══════════════════════════════");
        System.out.println("QUIZ ENDED, your score:");
        System.out.printf("Correct Answers: %d, Wrong Answers: %d, Rate: %.1f %s %n",
                (int) service.getCorrectAnswers(),
                (int) service.getWrongAnswers(),
                service.countRate(),
                "%");
        System.out.println("══════════════════════════════");
    }

    private static void readNumberOfQuestions() {
        System.out.println("Please provide number of questions: ");
        System.out.print("► ");
        int numOfQuestions = readNumberFromUser();

        while (numOfQuestions < 1) {
            if (numOfQuestions < 1) {
                System.out.println("Please select at least 1 question!");
                System.out.print("► ");
                numOfQuestions = readNumberFromUser();
            }
        }

        service.setNumberOfQuestions(numOfQuestions);
    }

    private static void readCategoryIndex() {
        System.out.println("Please select the category: ");
        System.out.print("► ");
        boolean isCorrect = true;
        int index = -1;

        while (isCorrect) {
            index = readNumberFromUser();
            index--;

            isCorrect = !(index >= 0 && index < service.getCategoriesList().size());

            if (isCorrect) {
                System.out.println("Wrong selection! Try again");
                System.out.print("► ");
            }
        }

        service.setCategory(service.getCategoriesNames().get(index));
    }

    private static int readNumberFromUser() {
        Scanner userInput = new Scanner(System.in);
        Integer readNumber = null;

        while (readNumber == null) {
            try {
                readNumber = Integer.parseInt(userInput.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Wrong number format! Try again.");
                System.out.print("► ");
            }
        }

        return readNumber;
    }

    private static void printTitle() {
        System.out.println("┌────────────────────────────┐");
        System.out.println("│ QUIZ APP ver. 1.0          │");
        System.out.println("└────────────────────────────┘");
    }

    private static void printMenu() {
        System.out.println("┌─────────────────────────────────────────┐");
        System.out.printf("│ %-39s │%n", "QUESTIONS CATEGORIES:");
        final int[] count = {1};

        service.getCategoriesNames()
                .forEach(cat -> System.out.printf("│ %2d) %-35s │%n", count[0]++, cat));

        System.out.println("└─────────────────────────────────────────┘");
    }
}
