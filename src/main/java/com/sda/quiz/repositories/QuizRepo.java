package com.sda.quiz.repositories;

import com.sda.quiz.models.QuizQuestion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class QuizRepo {
    private final static String QUESTIONS_FOLDER = "src/main/resources/db/";

    public static List<File> getListOfFiles() {
        File folder = new File(QUESTIONS_FOLDER);
        return Arrays.asList(folder.listFiles());
    }

    public static List<QuizQuestion> getCategoryQuestionsList(String category) {
        String url = QUESTIONS_FOLDER + category + ".txt";
        File file = new File(url);
        List<QuizQuestion> quizQuestionsList = new ArrayList<>();

        try {
            Scanner input = new Scanner(file);
            while (input.hasNext()) {
                QuizQuestion quizQuestion = new QuizQuestion();
                quizQuestion.setQuestion(input.nextLine());
                int answers = Integer.parseInt(input.nextLine());

                for (int i = 0; i < answers; i++) {
                    quizQuestion.addAnswer(i + 1, input.nextLine());
                }
                quizQuestionsList.add(quizQuestion);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }

        return quizQuestionsList;
    }
}
