package com.sda.quiz;

import com.sda.quiz.cli.QuizCli;

public class QuizApp {
    public static void main(String[] args) {
        QuizCli cli = new QuizCli();
        cli.runApp();
    }
}
